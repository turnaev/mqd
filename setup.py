#!/usr/bin/env python
# -*- coding: utf8 -*-

# ultraviolance distutils hack
import re
import distutils.versionpredicate
distutils.versionpredicate.re_validPackage = re.compile(r"^\s*([a-z_-]+)(.*)", re.IGNORECASE)

from distutils.core import setup

try:
    import xpkg
except ImportError:
    pass

packages = {
    'mqd' : 'src/mqd',
}


setup ( 
        name= 'mqd',
        version='0.3.1',
        packages=packages,
        package_dir = packages,
        author='Evgeny Turnaev',
        author_email='turnaev.e@gmail.com',
        description='Zmq daemon unitilies',
        long_description="""Common code that pulls messages from zmq sockets and runs user defined function to process received messages.""",

        classifiers = ['devel'],
        options={'build_pkg': {'name_prefix': True,
                               'python_min_version': 2.7,
                              }},
        requires = ['py-yaml',
                    'py-radist', 
                    'py-pyev', 
                    'py-pyzmq (>=2.1)', 
                    'py-cythstat (>=0.1.0)',
                    'py-xutils (>=0.1.3)'],
        provides = packages.keys(),
        )



