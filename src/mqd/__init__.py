#!/usr/bin/env python

import argparse
import yaml
from xutils import merge_dict_typesafe
import pprint

MQD_DEFAULTS = {
    'daemonize' : False,

    'mqd': {
        'pub_addr' : [],
        'pub_hwm' : 10000,
        'pull_from_cluster' : '',
        'pull_port' : 0,
        'connect_addrs': [],
        'bind_addrs': [],
    },

    'xstat': {
        'mem_type' : 'shared',
        'name' : '',
    }
}

def get_conf(default_conf):
    conf = merge_dict_typesafe(MQD_DEFAULTS, default_conf)

    arg_parser = argparse.ArgumentParser(description='Read message queue and perform http requests.')
    arg_parser.add_argument('--conf',
                            required = True
                            )

    arg_parser.add_argument("--pid",
                            help = "path to pid file."
                           )

    args = arg_parser.parse_args()

    with open(args.conf) as f:
        merge_dict_typesafe(conf, yaml.load(f.read()))
    merge_dict_typesafe(conf, dict(args._get_kwargs()))

    if not conf['daemonize']:
        pprint.pprint(conf)
    return conf



