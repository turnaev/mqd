#!/usr/bin/env python

import sys
import time
import os.path
import signal
from logging import debug, warning, info, error, critical

from msgpack import unpackb, packb
import pyev
import zmq
from zmq.core.error import ZMQBaseError
from zmq import EVENTS
from xutils.zmq_compat import SND_HWM, RCV_HWM

from xutils.pidfile import PidFile
from xutils.daemon import XDaemonContext

from struct import unpack

from xstat import stat


class MQPullDaemon(object):

    def __init__(self, conf):

        self.xstat = stat
        self._msg_origins = {}
        self.conf = conf
        self.mqd_conf = conf['mqd']

        self.pull_connect_addrs = []
        self.pull_bind_addrs = []
        if self.mqd_conf['pull_from_cluster'] and self.mqd_conf['pull_port']:
            try:
                import radist
                ix=radist.get_ix()
                ix_servers = getattr(ix, self.mqd_conf['pull_from_cluster'])
                self.pull_connect_addrs = [ "tcp://%s:%s" % (n._get_server(), self.mqd_conf['pull_port']) for n in ix_servers.get()]
            except ImportError:
                import warnings
                warnings.warn("no module radist found.")
        if self.mqd_conf['connect_addrs']:
            self.pull_connect_addrs.extend(self.mqd_conf['connect_addrs'])

        if self.mqd_conf['bind_addrs']:
            self.pull_bind_addrs.extend(self.mqd_conf['bind_addrs'])

        if not self.pull_connect_addrs and not self.pull_bind_addrs:
            raise ValueError("No pull addresses specified.")


    def process_msg(self, msg):
        raise NotImplementedError("process_msg")

    def worker_init(self):
        pass

    def exit_cb(self, watcher, events):
        debug("MQPullDaemon exit()")
        watcher.loop.stop(pyev.EVBREAK_ALL)


    def create_origin_xstat(self, msg_from):
        self.xstat.get('mqd:in_msg:%s:latency_sum' % msg_from, 'L')
        self.xstat.get('mqd:in_msg:%s:cnt' % msg_from, 'L')
        self.xstat.get('mqd:in_msg:%s:process_error' % msg_from, 'L')


    def _main(self, connect_addrs_list, bind_addrs_list):

        self.xstat.write_header(
            version = 0x01,
            master_pid = os.getpid(),
            instance_time = int(time.time()),
            worker_pid = os.getpid(),
            worker_time = int(time.time()),
        )

        ctx = zmq.Context()
        loop = pyev.default_loop()
        self.loop = loop
        debug("loop: %s" % loop)

        proto_error = self.xstat.get('mqd:mq_proto_error', 'L')

        def enqueue(watcher, events, zmq_pollin = zmq.POLLIN, stat = stat):
            try:
                zmq_socket = watcher.data
                while 1:
                    queue_events = zmq_socket.getsockopt(EVENTS)
                    if __debug__:
                        debug("zmq queue_events: %d" % queue_events)
                    if not queue_events & zmq_pollin:
                        if __debug__:
                            debug("zmq event is not read - break")
                        break
                    msg = zmq_socket.recv_multipart(copy = True)
                    if __debug__:
                        debug("received msg: %s" % repr(msg))

                    if self.pub_socket:
                        try:
                            self.pub_socket.send_multipart(msg)
                        except ZMQBaseError:
                            error("", exc_info = True)

                    try:
                        if len(msg) > 1:
                            proto_version, msg_from, msg_time, msg  = msg[0], msg[1], msg[2], msg[3:]
                            msg_time = unpack("!I", msg_time)[0]
                            proto_version = unpack("!I",proto_version)[0]
                            msg_raw = packb((proto_version, msg_from, msg_time, msg))
                        else:
                            msg_raw = msg[0]
                            proto_version, msg_from, msg_time, msg = unpackb(msg[0])
                    except:
                        error("", exc_info = True)
                        proto_error.inc()
                        continue

                    if msg_from not in self._msg_origins:
                        self.create_origin_xstat(msg_from)

                    stat.get('mqd:in_msg:%s:latency_sum' % msg_from, 'L').inc_by( int(self.loop.now()) - msg_time )
                    stat.get('mqd:in_msg:%s:cnt' % msg_from, 'L').inc()
                    try:
                        self.process_msg(msg_raw, proto_version, msg_from, msg_time, msg)
                    except:
                        error("Error processing message: %s" % repr(msg_raw), exc_info = True)
                        stat.get('mqd:in_msg:%s:process_error' % msg_from, 'L').inc()
            except:
                error("", exc_info = True)


        self.pub_socket = None
        if self.mqd_conf['pub_addr']:
            pub_socket = ctx.socket(zmq.PUB)
            pub_socket.setsockopt(SND_HWM, self.mqd_conf['pub_hwm'])
            debug("pub_socket HWM: %s" % repr(self.mqd_conf['pub_hwm']))
            pub_socket.setsockopt(zmq.LINGER,0)
            for addr in self.mqd_conf['pub_addr']:
                debug("Copy-Publish addr: %s" % repr(addr))
                pub_socket.bind(addr)
            self.pub_socket = pub_socket

        zmq_socket = ctx.socket(zmq.PULL)
        zmq_socket.setsockopt(RCV_HWM, 1)
        debug("PULL HWM set to: %s" % 1)
        zmq_sock_fd = zmq_socket.getsockopt(zmq.FD)
        queue_watcher = loop.io( zmq_sock_fd, pyev.EV_READ, enqueue, zmq_socket)
        queue_watcher.start()

        for addr in connect_addrs_list:
            try:
                zmq_socket.connect(addr)
            except:
                error("Invalid zmq address: %s" % repr(addr))
                raise
            debug("connect pull addr: %s zmq_sock_fd: %d" % (addr, zmq_sock_fd))

        for addr in bind_addrs_list:
            try:
                zmq_socket.bind(addr)
            except:
                error("Invalid zmq address: %s" % repr(addr))
                raise
            debug("bind pull addr: %s zmq_sock_fd: %d" % (addr, zmq_sock_fd))

        self.worker_init()
        debug("worker init done. staring loop.")
        sig_int_watcher = loop.signal(signal.SIGINT, self.exit_cb)
        sig_term_watcher = loop.signal(signal.SIGTERM, self.exit_cb)
        sig_int_watcher.start()
        sig_term_watcher.start()
        try:
            loop.start()
        except:
            error("", exc_info = True)
        debug("loop.start() returned.. application exit.")


    def run(self):

        logs_base = self.conf['log']['base_path']

        if self.conf['daemonize']:
            files_preserve = []
            stdout = open( os.path.join(logs_base, "stdout.log"),"ab")
            stderr = open( os.path.join(logs_base, "stderr.log"),"ab")
        else:
            files_preserve = [0,1,2]
            stdout = sys.stdout
            stderr = sys.stderr

        pidfile = None
        if self.conf['pid']:
            pidfile = PidFile(self.conf['pid'])

        context = XDaemonContext(
            detach_process = bool(self.conf['daemonize']),
            working_directory = '/',
            pidfile = pidfile,
            stdout = stdout,
            stderr = stderr,
            signal_map = {},
            files_preserve = files_preserve
                                      )
        with context:
            self._main(self.pull_connect_addrs, self.pull_bind_addrs)






