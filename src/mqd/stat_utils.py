
import os
import errno
from cythstat import unlink, SharedDict

def simple_open(conf):
    if conf['xstat']['mem_type'] == 'heap':
        result = SharedDict("", os.O_RDWR, 2)
    else:
        name = conf['xstat']['name']
        if not name:
            raise ValueError("xstat name must be set in order to work with --xstat-mem-type=shared")
        try:
            unlink(name)
        except Exception, e:
            if e[0] != errno.ENOENT:
                raise
        result = SharedDict(name, os.O_CREAT | os.O_RDWR, 1)
    return result


